#pragma once

#include <vector>
#include <string>
#include <memory>
#include "Heaps/IHeap.h"
#include "Heaps/UnevenHeap.h"
#include "Heaps/STLHeap.h"
#include "Heaps/BinomialHeap.h"

template <class theHeapType>
class HeapsManager {
public:

	//using theHeapType = LeftistHeap;

    //HeapsManager() : heaps() {}

	void AddHeap(type key);
	void Insert(size_t index, type key);
	type GetMin(size_t index);
	type ExtractMin(size_t index);
	void Meld(size_t index1, size_t index2);

    size_t size(){ return heaps.size(); }
    bool empty(){return heaps.empty();}
    bool empty(size_t index){ return heaps[index]->empty();}

private:
	std::vector<IHeap*> heaps;
};

template <class theHeapType>
void HeapsManager<theHeapType>::AddHeap(type key) {
    heaps.push_back(new theHeapType(key));
}

template <class theHeapType>
void HeapsManager<theHeapType>::Insert(size_t index, type key) {
    heaps[index]->Insert(key);
}

template <class theHeapType>
type HeapsManager<theHeapType>::GetMin(size_t index) {
    return heaps[index]->GetMin();
}

template <class theHeapType>
type HeapsManager<theHeapType>::ExtractMin(size_t index) {
    return heaps[index]->ExtractMin();
}

template <class theHeapType>
void HeapsManager<theHeapType>::Meld(size_t index1, size_t index2) {
    heaps[index1]->Meld((heaps[index2])); //need to right write how copy happen
}