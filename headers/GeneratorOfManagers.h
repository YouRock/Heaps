#include "HeapsManager.h"
#include <string>
class TestManager {


public:
    using TTestingHeap = LeftistHeap;

    HeapsManager<STLHeap> WorkingHeaps;
    HeapsManager<TTestingHeap> TestingHeaps;

    std::string NameOfTestingHeap(){
        if(typeid(TTestingHeap) == typeid(LeftistHeap))
            return "Leftist Heap";

        else if(typeid(TTestingHeap) == typeid(SkewHeap))
            return "Skew Heap";

        else if(typeid(TTestingHeap) == typeid(BinomialHeap))
            return "Binomial Heap";
    };

private:

    /*HeapsManager<STLHeap> GetWorkingHeap(){
        return HeapsManager<STLHeap>();
    }

    HeapsManager<TTestingHeap> GetTestingHeap(){
        return HeapsManager<TTestingHeap>();
    }*/
};