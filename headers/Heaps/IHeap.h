#pragma once

#include "Node.h"

class IHeap {
public:
	IHeap() = default;
	IHeap(const type& key);
	virtual ~IHeap() = default;

	virtual void Insert(const type& key) = 0;
	virtual type GetMin() = 0;
	virtual type ExtractMin() = 0;
	virtual void Meld(IHeap* right) = 0;
    virtual bool empty() = 0;

	//virtual node_ptr _Merge(node_ptr, node_ptr) = 0;

};
