#pragma once
#include <memory>
#include <algorithm>

typedef int type;

struct Node{
	using node_ptr = Node*;

    Node() = default;
    Node(Node&& N) : field(N.field), dist(N.dist), null(N.null),
                     left(std::move(N.left)), right(std::move(N.right)) {
    };

    Node(const Node& N) : field(N.field), dist(N.dist), null(N.null),
                          left((N.left)), right((N.right)) {
    };

    Node(const type& key) : field(key), null(false), left(nullptr), right(nullptr) {}
    ~Node(){
        if(right!= nullptr)
            right->~Node();

        delete right;

        if(left!= nullptr)
            left->~Node();

        delete left;

    }

    Node& operator=(Node n) {
        field = n.field;
        dist = n.dist;
        null = n.null;
        left = (n.left);
        right = (n.right);

        return *this;
    }

    type operator*() { return field; }
    type empty() const { return null; }

    virtual void Relax(){
        size_t distR = 1, distL = 1;

        if(right != nullptr)
            distR = right->dist + 1;

        if(left != nullptr)
            distL = left->dist + 1;

        dist = std::min(distL, distR);

        if (distL < distR)
            std::swap(left, right);
    }


    type field;
    size_t dist = 0;
    bool null = true;
    node_ptr left = nullptr;
    node_ptr right = nullptr;
};

