#pragma once

#include "Node.h"
#include "IHeap.h"
#include <memory>

struct SkewNode : public Node {
    SkewNode(const SkewNode& N) : Node(dynamic_cast<Node&>(const_cast<SkewNode&>(N))){
    };

    SkewNode(const type& key) : Node(key) {}

    virtual void Relax() override {
        std::swap(left, right);
    }
};

template<class TNode>
class UnevenHeap : public IHeap {//<UnevenHeap> {
public:
    using tnode_ptr = TNode*;

    UnevenHeap() = default;
    explicit UnevenHeap(const type& key);
    explicit UnevenHeap(UnevenHeap&& lh) : root(std::move(lh.root)) {};
    ~UnevenHeap(){
        root->~TNode();
        delete root;
    }

    virtual void Insert(const type& key) ;
    virtual type GetMin() ;
    virtual type ExtractMin() ;
    virtual void Meld(IHeap* right) ; //can i write "Meld(UnevenHeap)"????

    virtual bool empty(){ return root == nullptr;}

    TNode* GetRoot();

private:
    TNode* _Merge(tnode_ptr right, tnode_ptr left);
    tnode_ptr root = nullptr;
};



using LeftistHeap = UnevenHeap<Node>;
using SkewHeap = UnevenHeap<SkewNode>;

template<class TNode>
UnevenHeap<TNode>::UnevenHeap(const type& key) : root(new TNode(key)) {}

template<class TNode>
void UnevenHeap<TNode>::Meld(IHeap* ih) {  //do free function
    UnevenHeap<TNode>* other = dynamic_cast<UnevenHeap<TNode>*>(ih);
    tnode_ptr n = new TNode(*_Merge(root, other->GetRoot()));

    other->root = nullptr;



    root = n;
}

template<class TNode>
TNode* UnevenHeap<TNode>::_Merge(tnode_ptr left, tnode_ptr right) {

    if (left == nullptr)
        return right;

    if (right == nullptr)
        return left;

    if (**right < **left)
        std::swap(left, right);

    left->right = (_Merge(dynamic_cast<TNode*>(left->right), dynamic_cast<TNode*>(right)));

    left->Relax();

    return left;
}

template<class TNode>
void UnevenHeap<TNode>::Insert(const type & key) {
    TNode* tmp = new TNode(key);
    root = _Merge(root, tmp);
}

template<class TNode>
type UnevenHeap<TNode>::GetMin() {
    return **root;
}

template<class TNode>
type UnevenHeap<TNode>::ExtractMin() {
    type result = **root;
    root = _Merge(dynamic_cast<TNode*>(root->left), dynamic_cast<TNode*>(root->right));

    return result;
}

template<class TNode>
TNode* UnevenHeap<TNode>::GetRoot() {
    return root;
}