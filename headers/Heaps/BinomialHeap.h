#pragma once

#include "BinomialNode.h"
#include "IHeap.h"
#include <memory>


class BinomialHeap : public IHeap {//<BinomialHeap// > {
public:

    BinomialHeap() = default;
    BinomialHeap(const type& key) : root(new BinomialNode(key)) {}

    BinomialHeap(BinomialHeap && lh) : root(std::move(lh.root)) {}
    BinomialHeap(BinomialHeap& bh) : root(bh.root) {}
    BinomialHeap(binnode_ptr bp) : root(bp) {}
    virtual ~BinomialHeap(){
        root->~BinomialNode();
        root = nullptr;
    }

    virtual void Insert(const type& key) ;
    virtual type GetMin() ;
    virtual type ExtractMin() ;
    virtual void Meld(IHeap*);

    virtual bool empty(){return root == nullptr;}

    binnode_ptr GetRoot() {return root;};

private:
    binnode_ptr _Merge(binnode_ptr right, binnode_ptr left);
    void CheckStatus();

    binnode_ptr root;
};
