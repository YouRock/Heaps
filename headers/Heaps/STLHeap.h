#pragma once

#include <vector>
#include <algorithm>
#include <memory>
#include "Node.h"
#include "IHeap.h"

class STLHeap : public IHeap {
public:
    static bool Compare(const type&, const type&);

    STLHeap() = default;
    STLHeap(const type& key) : heap(1, key) {};
    virtual ~STLHeap() = default;

    virtual void Insert(const type& key) ;
    virtual type GetMin() ;
    virtual type ExtractMin() ;
    virtual bool empty(){return heap.empty();}

    virtual void Meld(IHeap*);
    //const std::vector<type>::const_iterator GetBegin() const;
    //const std::vector<type>::const_iterator GetEnd() const;
private:
    std::vector<type> heap;
};
