#pragma once
#include <memory>
#include <algorithm>

typedef int type;

struct BinomialNode{

    BinomialNode() = default;
    BinomialNode(BinomialNode&& N) : field(N.field), dist(N.dist), null(N.null),
                     son(std::move(N.son)), following(std::move(N.following)) {
    };

    BinomialNode(const BinomialNode& N) : field(N.field), dist(N.dist), null(N.null),
                          son((N.son)), following((N.following)), degree(N.degree) {
    };

    BinomialNode(const type& key) : field(key), null(false), son(nullptr), following(nullptr), degree(0) {}

    BinomialNode& operator=(BinomialNode n) {
        field = n.field;
        dist = n.dist;
        null = n.null;
        son = (n.son);
        following = (n.following);

        return *this;
    }

    type operator*() { return field; }
    type empty() const { return null; }

    void Relax(){
        size_t distR = 1, distL = 1;

        if(following != nullptr)
            distR = following->dist + 1;

        if(son != nullptr)
            distL = son->dist + 1;

        dist = std::min(distL, distR);

        if (distL < distR)
            std::swap(son, following);
    }


    size_t degree = 0;
    type field;
    size_t dist = 0;
    bool null = true;
    BinomialNode* son = nullptr;
    BinomialNode* following = nullptr;
};

using binnode_ptr = BinomialNode*;