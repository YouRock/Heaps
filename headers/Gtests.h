#include <iostream>
#include "GeneratorOfManagers.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <random>
#include <map>
#include <vector>
#include <string>
#include <ctime>
#include <algorithm>

type randomValue(){
    return rand() % (INT32_MAX-10);
}

//#define DEBUG

using std::string;
using std::vector;
using std::map;

TEST(ALL_TESTS, FullTest) {
    //map<string, vector<clock_t > > timeTable;
    //vector<string> actions {"AddHeap", "Insert", "GetMin", "ExtractMin", "Meld"};

    srand(time(NULL));

    int decision;

    TestManager manGen;
    std::cout<<"Test " << manGen.NameOfTestingHeap() <<std::endl;

    for (int i = 0; i < 10000; ++i) {
        decision = rand() % 5;

        clock_t startTime = 0, endTime = 0;
        //clock_t startTime = clock();
        switch (decision) {
            case 0: {
                int key = randomValue();

                manGen.TestingHeaps.AddHeap(key);
                manGen.WorkingHeaps.AddHeap(key);


                break;
            }

            case 1: {
                if (!manGen.WorkingHeaps.empty()) {
                    type key = rand() % (INT32_MAX - 10);
                    size_t index = rand() % manGen.WorkingHeaps.size();

                    manGen.TestingHeaps.Insert(index, key);
                    manGen.WorkingHeaps.Insert(index, key);

                    ASSERT_EQ(manGen.TestingHeaps.GetMin(index), manGen.WorkingHeaps.GetMin(index));
                }
                break;
            }

            case 2: {
                if (!manGen.WorkingHeaps.empty()) {
                    size_t index = rand() % manGen.WorkingHeaps.size();

                    if (!manGen.WorkingHeaps.empty(index)) {
                        type workHMin = manGen.WorkingHeaps.GetMin(index);
                        type testHMin = manGen.TestingHeaps.GetMin(index);

                        ASSERT_EQ(workHMin, testHMin);
                    }
                }
                break;
            }

            case 3: {
                if (!manGen.WorkingHeaps.empty()) {
                    size_t index = rand() % manGen.WorkingHeaps.size();

                    if (!manGen.WorkingHeaps.empty(index)) {
                        type workHMin = manGen.WorkingHeaps.ExtractMin(index);

                        type testHMin = manGen.TestingHeaps.ExtractMin(index);


                        ASSERT_EQ(workHMin, testHMin);
                    }
                }
                break;
            }

            case 4: {
                if (manGen.WorkingHeaps.size() > 1) {
                    size_t index1 = rand() % (manGen.WorkingHeaps.size() - 1);
                    size_t index2 = rand() % (manGen.WorkingHeaps.size() - index1 - 1) + index1;

                    if (index1 != index2 && !manGen.WorkingHeaps.empty(index1) && !manGen.WorkingHeaps.empty(index2)) {
#ifdef DEBUG
                        std::cout<<"Meld " << index1 << " with " << index2 << std::endl;
#endif //DEBUG

                        manGen.WorkingHeaps.Meld(index1, index2);

                        manGen.TestingHeaps.Meld(index1, index2);
                    }

                    ASSERT_FALSE(manGen.TestingHeaps.empty(index1) && !manGen.WorkingHeaps.empty(index1));



                    if(!manGen.WorkingHeaps.empty(index1)) {
#ifdef DEBUG
                        std::cout<<"Meld " << index1 << " with " << index2 << " heap. WorkMin = " << manGen.WorkingHeaps.GetMin(index1)
                                 << "TestMin = " << manGen.TestingHeaps.GetMin(index1) << std::endl;
#endif //DEBUG

                        ASSERT_EQ(manGen.TestingHeaps.GetMin(index1), manGen.WorkingHeaps.GetMin(index1));
                    }
                }
                break;
            }
        }


        /*if(startTime!=0 && endTime!=0)
            timeTable[actions[decision]].push_back((endTime - startTime));*/
    }



/*    for (int k = 0; k < actions.size(); ++k) {
        for (int j = 0; j < timeTable[actions[k]].size(); ++j) {
            answer += timeTable[actions[k]][j] / ld(timeTable[actions[k]].size());
        }

        //answer = *std::max_element(timeTable[actions[k]].begin(), timeTable[actions[k]].end());
        //std::cout << actions[k] << ": " << answer <<std::endl;
    }*/

}

TEST(ALL_TESTS, TimeTest){
    TestManager testMan;

    clock_t time = clock();
    for (int i = 0; i < 100000; ++i) {
        testMan.TestingHeaps.AddHeap(randomValue());
    }
    time = clock() - time;

    std::cout<<"100000 AddHeap time = " << time <<std::endl;

    time = clock();
    for (int i = 0; i < 1000000; ++i) {
        testMan.TestingHeaps.Insert(rand()%testMan.TestingHeaps.size(), randomValue());
    }
    time = clock() - time;

    std::cout<<"1000000 Insert time = " << time <<std::endl;

    time = clock();
    for (int i = 0; i < 1000000; ++i) {
        testMan.TestingHeaps.GetMin(rand()%testMan.TestingHeaps.size());
    }
    time = clock() - time;

    std::cout<<"1000000 GetMin time = " << time <<std::endl;

    time = clock();
    for (int i = 0; i < 1000000; ++i) {
        int index = rand()%testMan.TestingHeaps.size();
        if(!testMan.TestingHeaps.empty(index))
            testMan.TestingHeaps.ExtractMin(index);
        else
            i--;
    }
    time = clock() - time;

    std::cout<<"1000000 ExtractMin time = " << time <<std::endl;

    time = clock();
    for (int i = testMan.TestingHeaps.size()-1; i > 0; --i) {
        testMan.TestingHeaps.Meld(i-1, i);
    }
    time = clock() - time;

    std::cout<<"100000 Meld time = " << time <<std::endl;

}

clock_t TimeOfNOperations(TestManager& tm, char operation, size_t n){
    clock_t time;

    vector<string> actions {"AddHeap", "Insert", "GetMin", "ExtractMin", "Meld"};

    switch (operation){
        case'A':{
            time = clock();
            for (int i = 0; i < n; ++i) {
                tm.TestingHeaps.AddHeap(randomValue());
            }
            time = clock() - time;

            break;
        }

        case'I':{
            time = clock();
            for (int i = 0; i < n; ++i) {
                tm.TestingHeaps.Insert(rand()%tm.TestingHeaps.size(), randomValue());
            }
            time = clock() - time;

            break;
        }

        case'G':{
            time = clock();
            for (int i = 0; i < n; ++i) {
                tm.TestingHeaps.GetMin(rand()%tm.TestingHeaps.size());
            }
            time = clock() - time;

            break;
        }

        case'E':{
            time = clock();
            for (int i = 0; i < n; ++i) {
                int index = rand()%tm.TestingHeaps.size();
                if(!tm.TestingHeaps.empty(index))
                    tm.TestingHeaps.ExtractMin(index);
                else
                    i--;
            }
            time = clock() - time;

            break;
        }

        case'M':{
            for (int k = 0; k < 10; ++k) {
                for (int j = 0; j < n; ++j) {
                    tm.TestingHeaps.Insert(tm.TestingHeaps.size() - 1 - j, randomValue());
                }
            }

            time = clock();
            for (int i = tm.TestingHeaps.size()-1; i > 0; --i) {
                tm.TestingHeaps.Meld(i-1, i);
            }
            time = clock() - time;

            break;
        }
    }

    return time;

}

TEST(ALL_TESTS, LinearTime){
    TestManager tm;

    vector<string> actions {"AddHeap", "Insert", "GetMin", "ExtractMin", "Meld"};

    int maxDegrOf10 = 6;
    int startDeg = 2;

    for (int j = 0; j < 5; ++j) {
        vector<clock_t> measurements;

        std::cout<<actions[j]<<":"<<std::endl;

        int n = 10;
        for (int i = startDeg; i <= maxDegrOf10; ++i) {
            n *= 10;

            clock_t time = TimeOfNOperations(tm, actions[j][0], n);
            measurements.push_back(time);
        }

        (measurements[0] == 0) ? measurements[0] = 1 : measurements[0];

        for (int k = startDeg; k <= 6; ++k) {
            std::cout<<"Operations increased " << pow(10,k) << " times" <<std::endl
                     << "Time increased " << (double) measurements[k-startDeg]/measurements[0] << " times" <<std::endl;
        }

        std::cout<<std::endl;
    }
}