#include "../../headers/Heaps/STLHeap.h"

bool STLHeap::Compare(const type& first, const type& second) {
    return first > second;
}

void STLHeap::Insert(const type &key) {
    heap.push_back(key);
    std::push_heap(heap.begin(), heap.end(), Compare);
}

type STLHeap::GetMin() {
    return heap.front();
}

type STLHeap::ExtractMin() {
    type tmp = heap.front();

    std::pop_heap(heap.begin(), heap.end(), Compare);
    heap.pop_back();

    return tmp;
}

/*const std::vector<type>::const_iterator STLHeap::GetBegin() const {
    return heap.begin();
}

const std::vector<type>::const_iterator STLHeap::GetEnd() const {
    return heap.begin();
}*/

void STLHeap::Meld(IHeap* IH){
    STLHeap* other = dynamic_cast<STLHeap*>(IH);

    heap.insert(heap.begin(), other->heap.begin(), other->heap.end());
    std::make_heap(heap.begin(), heap.end(), Compare);

    other->heap.clear();
}