/*#pragma once

#include "Node.h"
#include "IHeap.h"
#include <memory>

class LeftistHeap : public IHeap {//<LeftistHeap> {
public:

	LeftistHeap() = default;
	explicit LeftistHeap(const type& key);
	explicit LeftistHeap(LeftistHeap&& lh) : root(std::move(lh.root)) {};

	virtual void Insert(const type& key) ;
	virtual type GetMin() ;
	virtual type ExtractMin() ;
	virtual void Meld(IHeap& right) ; //can i write "Meld(LeftistHeap)"????

    virtual bool empty(){ return root == nullptr;}

	node_ptr GetRoot();

private:
    node_ptr _Merge(node_ptr right, node_ptr left);
	node_ptr root;
};
*/