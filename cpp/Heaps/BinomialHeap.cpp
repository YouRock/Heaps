#include "../../headers/Heaps/BinomialHeap.h"

type BinomialHeap::GetMin()  {
    binnode_ptr tmp = root;

    type answer = **tmp;
    while (tmp != nullptr) {
        if (answer > **tmp)
            answer = **tmp;
        tmp = (*tmp).following;
    }

    return answer;
}

void BinomialHeap::Meld(IHeap* ih) {
    BinomialHeap* other = dynamic_cast<BinomialHeap*>(ih);

    root = _Merge(root, other->GetRoot());
    other->root = nullptr;
}

void BinomialHeap::Insert(const type& field) {
    BinomialHeap* temp = new BinomialHeap(field);
    Meld(temp);
    temp->root = nullptr;
}

binnode_ptr BinomialHeap::_Merge(binnode_ptr first, binnode_ptr second){
    binnode_ptr answer;

    if (first == nullptr)
        return second;

    binnode_ptr left = nullptr;
    binnode_ptr firstPtr = first;
    binnode_ptr secPtr = second;

    binnode_ptr temp;

    while (firstPtr != nullptr && secPtr != nullptr) {
        if (firstPtr->degree <= secPtr->degree) {
            left = firstPtr;
            firstPtr = firstPtr->following;
        } else {
            if (left != nullptr)
                left->following = secPtr;
            else
                root = secPtr;

            temp = secPtr->following;
            secPtr->following = firstPtr;

            left = secPtr;
            secPtr = temp;
        }
    }

    if (secPtr != nullptr)
        left->following = secPtr;


    //binnode_ptr oldRoot = root;

    CheckStatus();

    return root;
}

void RightDegLessThanLeftDeg(binnode_ptr& root, binnode_ptr& left, binnode_ptr& right, binnode_ptr& deeper);
void RightDegBiggerThanLeftDeg(binnode_ptr& root, binnode_ptr& left, binnode_ptr& right, binnode_ptr& deeper);
void RightLessEqThanLeft(binnode_ptr& root, binnode_ptr& left, binnode_ptr& right, binnode_ptr& deeper);
void RightBiggerThanLeft(binnode_ptr& root, binnode_ptr& left, binnode_ptr& right, binnode_ptr& deeper);

void BinomialHeap::CheckStatus() {
    if (root == nullptr)
        return;

    binnode_ptr left = root;
    binnode_ptr right = root->following;
    binnode_ptr deeper = nullptr;

    while (right != nullptr) {
        if (right->degree < left->degree) {
            RightDegLessThanLeftDeg(root, left, right, deeper);
        }

        if (left->degree != right->degree) {
            RightDegBiggerThanLeftDeg(root, left, right, deeper);
        }

        else if (left->field <= right->field) {
            RightLessEqThanLeft(root, left, right, deeper);
        }

        else {
            RightBiggerThanLeft(root, left, right, deeper);
        }
    }
}

void RightDegLessThanLeftDeg(binnode_ptr& root, binnode_ptr& left, binnode_ptr& right, binnode_ptr& deeper){
    if (deeper != nullptr)
        deeper->following = right;
    else
        root = right;

    left->following = right->following;
    right->following = left;

    std::swap(left, right);
};

void RightDegBiggerThanLeftDeg(binnode_ptr& root, binnode_ptr& left, binnode_ptr& right, binnode_ptr& deeper){
    deeper = left;
    left = right;
    right = right->following;
}

void RightLessEqThanLeft(binnode_ptr& root, binnode_ptr& left, binnode_ptr& right, binnode_ptr& deeper){
    binnode_ptr binptr = right->following;

    right->following = left->son;
    left->son = right;

    left->following = binptr;

    ++(left->degree);

    right = left->following;
}

void RightBiggerThanLeft(binnode_ptr& root, binnode_ptr& left, binnode_ptr& right, binnode_ptr& deeper){
    left->following = right->son;
    right->son = left;

    ++(right->degree);

    if (deeper != nullptr) //CHECK!! nullptr?
        deeper->following = right;
    else
        root = right;

    left = right;
    right = left->following;
}

type BinomialHeap::ExtractMin() {
    binnode_ptr left = nullptr;
    binnode_ptr leftMin = nullptr;

    binnode_ptr ptr = root;
    binnode_ptr ptrOnMin = root;

    type answer = ptr->field;

    while (ptr) {
        if (answer > ptr->field) {
            answer = ptr->field;
            ptrOnMin = ptr;
            leftMin = left;
        }

        left = ptr;
        ptr = ptr->following;
    }

    if (leftMin != nullptr)
        leftMin->following = ptrOnMin->following;
    else
        root = ptrOnMin->following;

    BinomialHeap son(ptrOnMin->son);
    ptr = ptrOnMin->son;

    left = nullptr;
    while (ptr != nullptr) {
        leftMin = ptr->following;
        ptr->following = left;

        left = ptr;
        son.root = left;

        ptr = leftMin;
    }

    //BinomialHeap

    Meld(&son);
    son.root = nullptr;

    return answer;
}

